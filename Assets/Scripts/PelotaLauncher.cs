﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaLauncher : FireTemplate
{
    public float throughForce;
    public int damage;

    public override void DestroySelf()
    {
        Destroy(this.gameObject);
    }

    public override void Fire()
    {
        GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            Debug.Log("He tocado un enemy");
            collision.gameObject.GetComponent<CreaperBehaviour>().Damage(damage);
            DestroySelf();
        }
    }

}
