﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Youwin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Countdown");
    }

    private IEnumerator Countdown()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(Random.Range(4, 3));
    }
}